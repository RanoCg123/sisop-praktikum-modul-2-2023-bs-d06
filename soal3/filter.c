#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <sys/wait.h>

#define link "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download"
#define namazip "players.zip"

void buatTim(int b, int g, int p)
{
    int k = 1;
    char newfile[200];
    sprintf(newfile, "/home/tom/Formasi_%d-%d-%d.txt", b, g, p);
    pid_t pid_file = fork();
    if (pid_file == 0)
    {
        execl("/usr/bin/touch", "/usr/bin/touch", newfile, NULL);
    }
    else
    {
        int status;
        waitpid(pid_file, &status, 0);
    }

    pid_t pid_kiper = fork();
    if(pid_kiper == 0)
    {
        pid_t pid_gelandang = fork();
        if(pid_gelandang == 0)
        {
            pid_t pid_penyerang = fork();
            if(pid_penyerang == 0)
            {
                pid_t pid_bek = fork();
                if(pid_bek == 0)
                {
                    sprintf(newfile, "ls -1 players/Bek/*.png | sort -n -r -t _ -k 4 | head -n %d >> /home/tom/Formasi_%d-%d-%d.txt", b, b, g, p);
                    execl("/usr/bin/sh", "/usr/bin/sh", "-c", newfile, NULL);
                }
                else
                {
                    sprintf(newfile, "ls -1 players/Penyerang/*.png | sort -n -r -t _ -k 4 | head -n %d >> /home/tom/Formasi_%d-%d-%d.txt", p, b, g, p);
                    execl("/usr/bin/sh", "/usr/bin/sh", "-c", newfile, NULL);
                }
            }
            else
            {
                sprintf(newfile, "ls -1 players/Gelandang/*.png | sort -n -r -t _ -k 4 | head -n %d >> /home/tom/Formasi_%d-%d-%d.txt", g, b, g, p);
                execl("/usr/bin/sh", "/usr/bin/sh", "-c", newfile, NULL);
            }
        }
        else
        {
            sprintf(newfile, "ls -1 players/Kiper/*.png | sort -n -r -t _ -k 4 | head -n %d >> /home/tom/Formasi_%d-%d-%d.txt", k, b, g, p);
            execl("/usr/bin/sh", "/usr/bin/sh", "-c", newfile, NULL);
        }
    }
    else
    {
        int status;
        waitpid(pid_file, &status, 0);
    }
}





int main() {
    DIR *dir;
    struct dirent *entry;
    char dir_name[] = "/home/tom/Desktop/prak2/players/";
    char search_str[] = "ManUtd"; 
    char namafile[100];

    int pid_wget = fork();
    if (pid_wget == 0) {
    int pid_unzip = fork();
    if (pid_unzip == 0) {
        execl("/usr/bin/wget", "/usr/bin/wget", link, "-O", namazip, NULL);
    } else {
        int status;
        waitpid(pid_unzip, &status, 0);
    }
        execl("/usr/bin/unzip", "/usr/bin/unzip", namazip, "-d", ".", NULL);
    } else {
        int status;
        waitpid(pid_wget, &status, 0);
    }
    remove(namazip);

    dir = opendir(dir_name);
    if (dir == NULL) {
        printf("Tidak dapat membuka direktori %s.\n", dir_name);
        return 1;
    }
    
    while ((entry = readdir(dir)) != NULL) {
        
        if (strstr(entry->d_name, search_str) != NULL && strcmp(entry->d_name, search_str) != 1) {
            strcpy(namafile, dir_name);
            strcat(namafile, entry->d_name);
            printf("Menyimpan %s.\n", namafile);
        
        } else{
            strcpy(namafile, dir_name);
            strcat(namafile, entry->d_name);
            if (remove(namafile) == 0) {
                printf("Menghapus %s.\n", namafile);
            } else {
                printf("Gagal menghapus %s.\n", namafile);
            }
        }
    }     
    closedir(dir);

    printf("Creating folder........\n");
    int ffolder = fork();
    char *args[] = {"mkdir", "./players/Bek", "./players/Gelandang", "./players/Kiper", "./players/Penyerang", NULL};
    if (ffolder == 0){
        printf("test");
         execvp(args[0], args);
         exit(1);
    }
    else
    {
        int status;
        wait(&status);
    }
    printf("Folder Created.\n");


    pid_t clasify1 = fork();
    if (clasify1 == 0)
    {
        pid_t clasify2 = fork();
        if(clasify2 == 0)
        {
            pid_t clasify3 = fork();
            if(clasify3 == 0)
            {
                pid_t clasify4 = fork();
                if(clasify4 == 0)
                {
                    execl("/bin/sh", "sh", "-c", "mv ./players/*Bek* ./players/Bek/", NULL);
                }
                else
                {
                    execl("/bin/sh", "sh", "-c", "mv ./players/*Gelandang* ./players/Gelandang/", NULL);
                }
            }
            else
            {
                execl("/bin/sh", "sh", "-c", "mv ./players/*Penyerang* ./players/Penyerang/", NULL);
            }
        }
        else
        {
            execl("/bin/sh", "sh", "-c", "mv ./players/*Kiper* ./players/Kiper/", NULL);
        }
    }
    else
    {
        int status;
        wait(&status);
    }
    printf("Succsesfully\n");

    int b = 4;
    int g = 4;
    int p = 2;
    
    buatTim(b,g,p);

    return 0;
    
}