# sisop-praktikum-modul-2-2023-BS-D06

# Anggota
5025211119	Gilang Aliefidanto

5025211056	I Gusti Agung Ngurah Adhi Sanjaya

5025211185	Rano Noumi Sulistyo

## Soal 1
### Penjelasan Umum
Pada soal, diketahui terdapat satu file pemograman bahasa C yang harus menjalankan beberapa tahap. Pertama, program harus mengunduh file 'binatang.zip'. Kemudian mengekstrak file tersebut. Setelah itu mengambil salah satu hewan secara acak untuk dicetak ke terminal (melakukan shift penjagaan terhadap hewan itu). Setelah itu program memindahkan file ke folder sesuai nama (habitat) pada nama file. Terakhir, program melakukan kompres kembali ketiga folder tersebut.

#### Penjelasan Program (binatang.c)
Pada function main, diperlukan beberapa function untuk memperjelas kegunaan beberapa perintah dalam program. Function yang dibutuhkan adalah function untuk kebutuhan download file, membuat folder, kompres file, ekstrak file, memindahkan file, dan memilih nama file secara acak. Sehingga ada 6 function pada function main

__Function pada "int main()"__
```
void zip(char*, char*)
--untuk mengkompres file
void random_pick()
--untuk mengambil nama file secara acak
void unzip()
--untuk mengekstrak file
void file_to_new_dir() --> void move_file(char*, char*)
--untuk memindahkan file sesuai habitat yang telah ditentukan pada "file_to_new_dir()"
void create_folder()
--untuk membuat folder
void download()
--untuk mengunduh file
```
Semua function kecuali 'download' dibuat fork agar mencegah terjadinya suatu variabel yang masih digunakan ataupun sibuk. Selain itu, function yang memiliki perintah 'exec' juga dibuat fork untuk mencegah hal yang sama. Jadi fungsi dari fork adalah untuk menunggu *child* selesai mengerjakan tugasnya, kemudian *parent* boleh menjalankan tugasnya. Pada child di function 'main', juga diberi perintah 'exit(0);' untuk memberi sinyal bahwa child telah selesai mengerjakan tugasnya. Tiap fork diakhiri dengan perintah looping 'while(wait(NULL) != [variabel_fork]);' untuk membatasi jika perintah setelah looping tersebut tidak boleh dijalankan hingga child selesai bekerja. Sehingga, function 'main' ditulis secara lengkap adalah sebagai berikut:
```
int main (void) {
  pid_t main_fork;
  download();
  main_fork = fork();
  if(main_fork == 0) {
    create_folder();
    exit(0);
  }
  while(wait(NULL) != main_fork);
  main_fork = fork();
  if(main_fork == 0) {
    unzip();
    exit(0);
  }
  while(wait(NULL) != main_fork);

  main_fork = fork();
  if(main_fork == 0) {
    random_pick();
    exit(0);
  }
  while(wait(NULL) != main_fork);

  main_fork = fork();
  if(main_fork == 0) {
    file_to_new_dir();
    exit(0);
  }
  while(wait(NULL) != main_fork);

  main_fork = fork();
  if(main_fork == 0) {
    zip("HewanDarat.zip", "HewanDarat");
    zip("HewanAmphibi.zip", "HewanAmphibi");
    zip("HewanAir.zip", "HewanAir");
    exit(0);
  }
  while(wait(NULL) != main_fork);

  return 0;
}
```

##### download()
Pada function 'download' mengeksekusi perintah yang dapat berjalan pada terminal untuk mengunduh file. Cara mengunduh file menggunakan wget dengan format.
```
wget [link] -O [nama_file]
```
Pada function ini terdapat fork karena terdapat perintah exec. Sehingga function download ditulis sebagai berikut:
```
void download() {
  pid_t dlfork;
  dlfork = fork();
  char *argv[] = {"wget", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL};
  if(dlfork == 0) {
    execv("/usr/bin/wget", argv);
  }
  while(wait(NULL) != dlfork);
}
```

##### create_folder()
Pada function 'create_folder' juga mengeksekusi perintah yang dapat berjalan pada terminal, dengan format
```
mkdir kebun HewanDarat HewanAmphibi HewanAir
```
Folder kebun untuk kumpulan hewan yang ada di kebun binatang
Folder HewanDarat untuk hewan yang hidup di darat
Folder HewanAmphibi untuk hewan yang hidup di dua alam (darat dan air)
Folder HewanAir untuk hewan yang hidup di air
Pada function ini terdapat fork karena terdapat perintah exec. Sehingga function download ditulis sebagai berikut:
```
void create_folder() {
  pid_t mkdfork;
  mkdfork = fork();
  char *argv[] = {"mkdir", "kebun", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
  if(mkdfork == 0) {
    execv("/bin/mkdir", argv);
  }
  while(wait(NULL) != mkdfork);
}
```

##### unzip()
Pada function 'unzip' juga mengeksekusi perintah yang dapat berjalan pada terminal, dengan format
```
unzip -q binatang.zip -d kebun
```
-q = quite process
-d = direction
Pada function ini terdapat fork karena terdapat perintah exec. Sehingga function download ditulis sebagai berikut:
```
void unzip() {
  pid_t uzfork;
  uzfork = fork();
  char *argv[] = {"unzip", "-q", "binatang.zip", "-d", "kebun", NULL};
  if(uzfork == 0) {
    execv("/bin/unzip", argv);
  }
  while(wait(NULL) != uzfork);
  remove("binatang.zip");
}
```

##### random_pick()
Function 'random_pick' digunakan untuk memilih nama file secara acak dan dicetak sebagai hewan yang akan dijaga. Menggunakan data type 'DIR' dan struct, diberikan juga library 'dirent.h'. Kemudian dilakukan *looping* untuk menghitung jumlah file yang ada di folder kebun. Selain itu, harus ditambah 'srand(time(NULL))' dengan library 'time.h' untuk mendapatkan angka secara acak. Sehingga variabel yang berhubungan dengan opendir akan direset menggunakan 'rewinddir()' untuk dihitung ulang. Setelah mendapatkan angka dari hasil modular 'rand()' terhadap jumlah file, malah akan dihitung dari awal hingga menyentuh angka yang sesuai dengan hasil modular tersebut.
DT_REG = file biasa
```
void random_pick() {
  DIR *opdir = opendir("./kebun");
  struct dirent *rdir;
  if(opdir != NULL) {
    int total_files = 0;
    while((rdir = readdir(opdir)) != NULL ) {
      if(rdir->d_type == DT_REG) {
	    total_files++;
      }
    }
    srand(time(NULL));
    int num_rand = rand() % total_files;
    rewinddir(opdir);
    int count = 0;
    while((rdir = readdir(opdir)) != NULL) {
      if(rdir->d_type == DT_REG) {
	    if(count == num_rand) {
	      printf("Hewan yang dijaga saat ini adalah %s\n", rdir->d_name);
	    }
	    count++;
      }
    }
    closedir(opdir);
  }
}
```

##### file_to_new_dir() dan move_file(char* , char*)
Function 'file_to_new_dir()' untuk mengklasifikasi file, sehingga dapat digunakan ke function 'move_file(char* , char*)' untuk file yang telah diklasifikasi dipindahkan ke folder yang benar. Sama seperti 'random_pick()', 'file_to_ner_dir()' terdapat data type DIR, struct, dan array berisi 'kebun/' yang nantinya akan digabung dengan nama file (variabel 'd_name'). Setelah dilakukan filter menggunakan condition if-else, maka nama tujuan dan lokasi file dimasukan ke function 'move_file(char* , char*)'
'_darat,jpg' --> 'HewanDarat/'
'_amphibi,jpg' --> 'HewanAmphibi/'
'_air,jpg' --> 'HewanAir/'
```
void file_to_new_dir() {
  DIR *opdir = opendir("./kebun");
  struct dirent *rdir;
  if(opdir != NULL) {
    while((rdir = readdir(opdir)) != NULL) {
      char location[255] = "kebun/";
      strcat(location, rdir->d_name);
      if(strstr(rdir->d_name, ".jpg") != NULL) {
        if(strstr(rdir->d_name, "darat") != NULL) {
	  move_file("HewanDarat/", location);
	}
	else if(strstr(rdir->d_name, "amphibi") != NULL) {
          move_file("HewanAmphibi/", location);
        }
        else if(strstr(rdir->d_name, "air") != NULL) {
          move_file("HewanAir/", location);
        }
      }
    }
    closedir(opdir);
  }
  remove("kebun");
}
```

Function 'move_file(char* , char*)' hanya untuk proses pemindahan file. Namun dibutuhkan perintah yang dapat dijalankan pada terminal, dengan format
```
mv [location] [destination]
```
[location] = lokasi file beserta namanya
[destination] = lokasi tujuan file
Pada function ini terdapat fork karena terdapat perintah exec. Sehingga function download ditulis sebagai berikut:
```
void move_file(char *destination, char *location) {
  pid_t mvfork;
  mvfork = fork();
  char *argv[] = {"mv", location, destination, NULL};
  if(mvfork == 0) {
    execv("/bin/mv", argv);
  }
  while(wait(NULL) != mvfork);
}
```

##### zip(char*, char*)
Pada function 'zip' juga mengeksekusi perintah yang dapat berjalan pada terminal, dengan format
```
zip -q -rm [zipfile] [target]
```
-q = quite process
-rm = remove file
[zipfile] = nama zip yang disiapkan
[target] = file/folder yang akan dikompres
Pada function ini terdapat fork karena terdapat perintah exec. Sehingga function download ditulis sebagai berikut:
```
void zip(char *zipfile, char *target) {
  pid_t zfork;
  zfork = fork();
  char *argv[] = {"zip", "-q", "-rm", zipfile, target, NULL};
  if(zfork == 0) {
    execv("/usr/bin/zip", argv);
  }
  while(wait(NULL) != zfork);
}
```
## Soal 2
### Penjelasan Umum
Dalam soal ini kita diminta untuk melakukan 2 hal utama. Pertama membuat sebuah daemon process yang akan pertama, membuat dir baru. masuk ke dir berikut dan mendownload 15 foto dengan delay dan spesifikasi size berdasarkan waktu. setelah 15 foto didownload kita langsung mengzip folder dan mendelete folder tersebut. Hal kedua kita perlu membuat file program baru yang akan mematikan process daemon. Cara mematikan process nya akan dibagi 2 perdasarkan input saat menjalankan program utama. satu membuat process menjadi orphan process dan kedua mematikan semua process yang ada.
### Daemon process
Kita akan melakukan hal pertama yaitu pembuatan process daemon. 
```
pid_t pd, sd;
char fname[30], *type[]={"-a","-b"},mode[101],kp[101];

  pd = fork();
 if (pd < 0) {
    exit(EXIT_FAILURE);
  }
  if (pd > 0) {
    exit(EXIT_SUCCESS);
  }
  umask(0);
  sd = setsid();
  if (sd < 0) {
    exit(EXIT_FAILURE);
  }
```
kita perlu membuat sid dan pid dan melakukan fork yang akan menjadi parent process daemon. umask agar memiliki permission. lalu kita perlu membuat fork lagi untuk melakukan semua perintah perintahnya
####Pembuatan DIR
```
time_t ctime = time(NULL);
struct tm *tnow = localtime(&ctime);
strftime(fname,80,"%Y-%m-%d_%H:%M:%S", tnow);
```
Direktori yang dibuat memiliki format [YYYY-MM-dd_HH:mm:ss] berdasarkan waktu local. oleh karena itu kita perlu menggunakan library time.h agar bisa mendapatkan local device time. 
Kumpulan command diatas akan mendapatkan local time device dan akan membuat string dengan format [YYYY-MM-dd_HH:mm:ss] dengan nama string fname. 
setalah mendapatkan format nama directory kita bisa mendownload image. 
```
void mkfile(char *fname){
pid_t pd;
pd = fork();
char *argv[] = {"mkdir", "-p", fname, NULL};
  if(pd == 0){
      dlimage(fname);
  } 
  else{ 
    execv("/bin/mkdir", argv);
  }
}
```
kita pertama perlu melakukan fork process agar kita bisa mengeksekusi command mkdir. agar format jelas kita perlu membuat string argv yang berisi command dan dalam parent kita akan membuat mkdir dengan execv, yang memiliki format "library" dan "command". dalam forknya kita bisa melakukan download image. disini kita bisa melihat mengapa exekusi di parent agar kita bisa memastikan download terjadi setelah dir dibuat
#### Download image
```
void dlimage(char *fname){
char gname[30], path[100], dl[100];
pid_t pd;
for(int i = 0; i < 15; i++)
    {
            time_t ct = time(NULL);
            struct tm* tn = localtime(&ct);
            strftime(gname, 30, "%Y-%m-%d_%H:%M:%S", tn);
            sprintf(path, "./%s/%s.jpg", fname,gname);
            int size=(ct%1000)+50;
            sprintf(dl, "https://picsum.photos/%d", size);
            char *args[] = {"wget", "-q", "-O", path, dl, NULL};
        pd = fork();
        if(pd == 0)
        {
            execv("/bin/wget", args);
        }
        sleep(5);
    }
    
}
```
Disini terdapat fungsi download image yang memerlukan nama gambar, path gambar dan link download. Disini terdapat spesifik yang nama gambar memiliki format [YYYY-MM-dd_HH:mm:ss] yang sama dengan directory. Namun dikarenakan bisa ada delay dalam detik antara download dengan pembuatan dir atau dengan download kedua kita perlu mengambil local time baru dalam loop download yang dilakukan 15 kali. kita lalu mengguanakan srtftime lagi untuk mengaplikasikan format [YYYY-mm-dd_HH:mm:ss] ke string gname. lalu kita menggunakan sprintf untuk membuat string path yang berisi path dan nama dari file gambar. Kita juga diminta untuk mendownload gambar dari https://picsum.photos/, link ini memiliki detail diamana jika kita menulis angka disebelah link maka akan mendownload gambar dengan spesifikasi size angka tersebut. Hal ini akan membantu kita untuk melakukan download yang diminta, yaitu dengan gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel. buat link dengan spesifikasi size dan kita buat string args yang akan berisi command wget yang akan dieksekusi. Tentu saja agar bisa dieksekusi dan tidak berhenti perlu dilakukan fork dan eksekusi. setelah itu baru kita sleep selama 5 detik sesuai yang diminta.
#### Zip dan remove
```
void zpfile(char* fname)
{
    char zfile[50];
    sprintf(zfile, "%s.zip", fname);
    pid_t zfork,rfork;
    zfork = fork();
    char *argv[] = {"zip","-q", "-r", zfile, fname, NULL};
    if(zfork == 0)
    {
         execv("/bin/zip", argv );
    }
    while(wait(NULL)!=zfork);
    rmfile(fname);
}
```
Kita buat fungsi zpfile yang akan membuat zip. buat nama zip dengan format sesuai dengan sprintf yang sama dengan nama dir berupa string sfile. kita lalu buat fork dan sting argv berisi command zip. dalam fork lalu kita eksekusi. Kita lalu wait sebelum meremove file.
```
void rmfile(char* fname)
{
    char *argv[] = {"rm", "-r", fname, NULL};
    execv("/bin/rm", argv);
}
```
Kita buat argv untuk command rm dengan format rm -r agar delete semua dan di exekusi.
#### Hasil daemon
```
make = fork();
if(make == 0)
    {
   mkfile(fname);
   zpfile(fname);
    }

sleep(30);
}
```
setelah semua itu kita sleep 30 detik sesuai yang diminta. 
```
rano        3067  0.0  0.0   2772    88 ?        Ss   22:12   0:00 ./lukisan -a
rano        3070  0.0  0.0   2772   100 ?        S    22:12   0:00 ./lukisan -a

```
dalam computer process akand menjadi seperti ini Ss yaitu parent daemon dan S berupa child yang mendownload individual
### KILLER FILE
Kita perlu membuat file yang dapat mematikan process daemon. Agar bisa mematikan prosess kita perlu mengetahui pid atau nama dari process tersebut. Nah agar kita bisa melakukan orphan atau kill semua. DIsini kita perlu melihat input mode yang ada saat program di run -a untuk kill semua dan -b untuk orphan. Oleh karena itu kita perlu membaca input tersebut dengan `int main(int argc, char *argv[])`. disini argc untuk banyak string input tambahan dan argv sebagai isi dari string string tersebut sebagai array. 
```
  char fname[30], *type[]={"-a","-b"},mode[101],kp[101];
  if (strcmp(argv[1],type[0])==0){
 sprintf(mode,"char *argv[] = {\"killall\",\"-q\",\"lukisan\", NULL};");
 sprintf(kp,"/bin/killall");}
if (strcmp(argv[1],type[1])==0){
sprintf(mode,"char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
sprintf(kp,"/bin/kill");}
```
Terdapat string mode yang berisi command kill sesuai dengan input dan kp yang berisi library sesuai input juga. kita menngunakan strcmp untuk mengecek apakah itu -a atau -b dan mode dan kp akan sesuai dengan keinginan. 
```
void makekill(char order[], char kp[]){
 FILE *kl;
 char isi[]= "#include <stdio.h>\n" "#include <unistd.h>\n"
     "int main() {\n"
        "pid_t kill = fork();\n"
        "if (kill == 0) {\n"
            "%s"
            "execv(\"%s\", argv);\n"
        "}\n"
        "pid_t ks = fork();\n"
        "if (ks == 0) {\n"
        "char *argv[] = {\"rm\", \"kys\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"
         "}\n"
        "pid_t kys = fork();\n"
        "if (kys == 0) {\n"
        "char *args[] = {\"rm\", \"kys.c\", NULL};\n"
        "execv(\"/bin/rm\", args);\n"
        "}\n"
        "return 0;\n"
    "}\n";
 kl = fopen("kys.c", "w");
 fprintf(kl, isi, order, kp);
 fclose(kl);
 pid_t kys;
 kys=fork();
 if(kys==0){
char *argv[]={"gcc","kys.c","-o","kys", NULL};
execv("/bin/gcc",argv);
}
}
```
kita perlu 2 tersebut untuk membuat kode killer. pertama kita perlu membuat variable FILE bernaka kl, lalu string isi dari file tersebut. dengan %s yang bisa disi dengan mode dan kp agar bisa membuat 2 file yang berbeda sesuai input. kita lalu menentukan nama file dengan fopen dan fprintf untuk memasukkan isi, mode dan kp ke kl dan kita fclose. Setelah itu kita perlu membuat fork yang akan melakukan compile file yang namanya telah ditentukan sebelumnya.  Dalam file tersebut perlu buat fork dan mengeksekusi command kill sesuai mode. lalu kita buat fork yang akan mengeksekusi command untek meremove hasil compile file dan fork lain untuk meremove file killer tersendiri.
### KODE
```
void makekill(char order[], char kp[]){
 FILE *kl;
 char isi[]= "#include <stdio.h>\n" "#include <unistd.h>\n"
     "int main() {\n"
        "pid_t kill = fork();\n"
        "if (kill == 0) {\n"
            "%s"
            "execv(\"%s\", argv);\n"
        "}\n"
        "pid_t ks = fork();\n"
        "if (ks == 0) {\n"
        "char *argv[] = {\"rm\", \"kys\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"
         "}\n"
        "pid_t kys = fork();\n"
        "if (kys == 0) {\n"
        "char *args[] = {\"rm\", \"kys.c\", NULL};\n"
        "execv(\"/bin/rm\", args);\n"
        "}\n"
        "return 0;\n"
    "}\n";
 kl = fopen("kys.c", "w");
 fprintf(kl, isi, order, kp);
 fclose(kl);
 pid_t kys;
 kys=fork();
 if(kys==0){
char *argv[]={"gcc","kys.c","-o","kys", NULL};
execv("/bin/gcc",argv);
}
}
void dlimage(char *fname){
char gname[30], path[100], dl[100];
pid_t pd;
for(int i = 0; i < 15; i++)
    {
            time_t ct = time(NULL);
            struct tm* tn = localtime(&ct);
            strftime(gname, 30, "%Y-%m-%d_%H:%M:%S", tn);
            sprintf(path, "./%s/%s.jpg", fname,gname);
            int size=(ct%1000)+50;
            sprintf(dl, "https://picsum.photos/%d", size);
            char *args[] = {"wget", "-q", "-O", path, dl, NULL};
        pd = fork();
        if(pd == 0)
        {
            execv("/bin/wget", args);
        }
        sleep(5);
    }
    
}
void mkfile(char *fname){
pid_t pd;
pd = fork();
char *argv[] = {"mkdir", "-p", fname, NULL};
  if(pd == 0){
      dlimage(fname);
  } 
  else{ 
    execv("/bin/mkdir", argv);
  }
}
void rmfile(char* fname)
{
    char *argv[] = {"rm", "-r", fname, NULL};
    execv("/bin/rm", argv);
}
void zpfile(char* fname)
{
    char zfile[50];
    sprintf(zfile, "%s.zip", fname);
    pid_t zfork,rfork;
    zfork = fork();
    char *argv[] = {"zip","-q", "-r", zfile, fname, NULL};
    if(zfork == 0)
    {
         execv("/bin/zip", argv );
    }
    while(wait(NULL)!=zfork);
    rmfile(fname);
}




int main(int argc, char *argv[]){
pid_t pd, sd;
char fname[30], *type[]={"-a","-b"},mode[101],kp[101];

  pd = fork();
 if (pd < 0) {
    exit(EXIT_FAILURE);
  }
  if (pd > 0) {
    exit(EXIT_SUCCESS);
  }
  umask(0);
  sd = setsid();
  if (sd < 0) {
    exit(EXIT_FAILURE);
  }
  if (strcmp(argv[1],type[0])==0){
 sprintf(mode,"char *argv[] = {\"killall\",\"-q\",\"lukisan\", NULL};");
 sprintf(kp,"/bin/killall");}
if (strcmp(argv[1],type[1])==0){
sprintf(mode,"char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
sprintf(kp,"/bin/kill");}
makekill(mode,kp);
close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);
while(1){
pid_t make;
time_t ctime = time(NULL);
struct tm *tnow = localtime(&ctime);
strftime(fname,80,"%Y-%m-%d_%H:%M:%S", tnow);
make = fork();
if(make == 0)
    {
   mkfile(fname);
   zpfile(fname);
    }

sleep(30);
}
return 0;
}
```

## Soal 3
### Penjelasan Umum
Pada soal, di ketahui ada 1 program bernama filter.c yang akan menjalankan beberapa steps soal. 
Step pertama, program akan melakukan pengunduhan file zip dari sebuah link yang sudah di sediakan dan program akan mengekstrak file zip tersebut serta setelah di ekstrak, hasil ekstrak akan di simpan disuatu folder dan file zip akan di hapus.
Step kedua, program akan diminta untuk melakukan penyortiran nama pemain selain dari Manchester United akan di hapus dari folder.
Step ketiga, program akan membuat 4 folder dengan kategori nama Kiper, Bek, Gelandang, dan Penyerang. Lalu dari hasil step kedua, program diminta untuk memasukan nama file yang sudah di filter ke dalam masing masing folder yang sudah di buat pada step ini.
Step keempat, program akan melakukan penyortiran kembali dengan menemukan rating score terbaik dari masing masing kategori pemain (Kiper, Bek, Gelandang, dan Penyerang). Lalu program diminta menuliskan hasil output dari pemain terbaik di tiap tiap kategori dengan jumlah pemain yang di tentukan dan di masukan kedalam file .txt.

__Penjelasan Code__
##### Step 1
Pertama-tama, Define link dan nama dari file yang akan kita unduh.

```
#define link "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download"
#define namazip "players.zip"
```

Lalu masuk ke dalam int main(), terdapat variabel pointer dir dan entry yang berfungsi sebagai merepresentasikan sebuah direktori pada sistem operasi dan merepresentasikan sebuah file atau direktori pada sebuah direktori.
Terdapat juga dir_name[] = "/home/tom/Desktop/prak2/players/"; untuk mendeclare nama dari directory, char search_str[] = "ManUtd"; untuk mendeclare nama dari pemain yang akan di sortir nanti, char namafile[100]; untuk menyimpan nama file yang akan digunakan.

```
DIR *dir;
struct dirent *entry;
char dir_name[] = "/home/tom/Desktop/prak2/players/";
char search_str[] = "ManUtd"; 
char namafile[100];
```

Pada code berikut, di gunakan untuk melakukan pengunduhan file dan mengekstrakan(unzip)file yang sudah di unduh.
Pertama kita akan melakukan fork yang dimana akan menghasilkan child process. Pada child process di pid_wget akan melakukan fork kembali untuk unzip dan menghasilkan child process. Sebelum child process dari pid_unzip dilakukan, program pertama-tama akan menjalankan parent process nya terlebih dahulu di dalam else, yang dimana berisi code :
```
int status;
waitpid(pid_unzip, &status, 0);
```
Pada parent process pid_unzip, akan menunggu child process nya selesai terlebih dahulu baru parent processnya akan di execute.

Didalam child process dari pid_unzip akan melakukan pengunduhan file dengan execl dengan parameter :
```
execl("/usr/bin/wget", "/usr/bin/wget", link, "-O", namazip, NULL);
```
Program c akan melakukan fungsi execl yang dimana akan menggantikan proces yang terjadi sebelumnya dengan process yang baru. Didalam execl ini terdapat fungsi pada linux yaitu wget, lalu akan megambil url file yang sudah didefine dengan nama "link", lalu -o adalah fungsi dari wget untuk menentukan nama file yang akan digunakan dari hasil pengunduhan wget, -o mengambil nama dari namazip yang sudah di define di awal, lalu NUll.

Setelah proses pengunduhan selesai, akan dilakukan proses unzip juga di dalam child process pid_wget dengan code berikut :
```
execl("/usr/bin/unzip", "/usr/bin/unzip", namazip, "-d", ".", NULL);
```
"execl" akan menjalankan fungsi unzip dari linux dengan mencari nama file zip "namazip" lalu "-d" adalah fungsi dari unzip untuk menentukan direktori tujuan extract dan "." menunjukan directory saat ini sebagai direktori tujuan extract, lalu NULL.
Lalu pada parent process dari pid_wget akan menunggu unzipping file yang dilakukan child processnya selesai terlebih dahulu, baru parentnya akan di execute.

Setelah kedua fork dari "pid_wget" dan "pid_unzip" selesai dilakukan, "remove(namazip);" akan melakukan mengahapusan file zip yang sudah di unduh dengan nama file zip yang di define dengan "namazip". 
```
int pid_wget = fork();
    if (pid_wget == 0) {
    int pid_unzip = fork();
    if (pid_unzip == 0) {
        execl("/usr/bin/wget", "/usr/bin/wget", link, "-O", namazip, NULL);
    } else {
        int status;
        waitpid(pid_unzip, &status, 0);
    }
        execl("/usr/bin/unzip", "/usr/bin/unzip", namazip, "-d", ".", NULL);
    } else {
        int status;
        waitpid(pid_wget, &status, 0);
    }
    remove(namazip);
```

##### Step 2
Pertama-tama kita akan melakukan fungsi untuk membukan direktori tujuan kita dengan "dir = opendir(dir_name);" yang dimana dir = open dir itu merupakan fungsi dari library dirent.h yang sudah di declare di awal int main. Lalu parameter dari opendir yaitu dir_name juga sudah di declare di awal dengan nama "char dir_name[] = "/home/tom/Desktop/prak2/players/";"

Lalu akan program akan mengecek apakah fungsi dir bisa dilakukan, jika bisa dia akan melakukan fungsi di luar if, jika tidak fungsi di dalam if akan di jalankan dan program akan terminate.
```
if (dir == NULL) {
        printf("Tidak dapat membuka direktori %s.\n", dir_name);
        return 1;
    }
```

Jika dir != NULL, maka program akan melakukan while dengan parameter "strstr(entry->d_name, search_str) != NULL && strcmp(entry->d_name, search_str) != 1" yang akan mencari string pada search_str (yang dimana sudah di define di awal dengan keyword "ManUtd") dengan entry->d_name yang beroutput nama file pada directory yang sudah ditentukan, yang akan bernilai true. Lalu "strcmp(entry->d_name, search_str) != 1" akan mengcompare search_str dengan entry->d_name dengan nilai true.

Jika kedua parameter itu bernilai true, maka akan menjalakan "strcpy(namafile, dir_name);" menyalin isi string dari "dir_name" (/home/tom/Desktop/prak2/players/) dengan isi string dari "namafile". Lalu strcat(namafile, entry->d_name); akan menggabungkan isi dari "entry->d_name"  ke dalam namafile. Jika sudah program akan mencetak "Menyimpan (namafile)".

Jika kedua parameter itu bernilai false, maka program akan melakukan hal yang sama dengan bagian diatas, namu program akan menghapus namafile jika remove(namafile) == 0 dan mengeluarkan output "Menghapus (namafile)", jika tidak program akan mengeluarkan "Gagal menghapus (namafile)".

Lalu closeddir(dir); merupakan fungsi yang digunakan untuk menutup direktori yang sudah kita buka di awal.

```
dir = opendir(dir_name);
    if (dir == NULL) {
        printf("Tidak dapat membuka direktori %s.\n", dir_name);
        return 1;
    }
    
    while ((entry = readdir(dir)) != NULL) {
        
        if (strstr(entry->d_name, search_str) != NULL && strcmp(entry->d_name, search_str) != 1) {
            strcpy(namafile, dir_name);
            strcat(namafile, entry->d_name);
            printf("Menyimpan %s.\n", namafile);
        
        } else{
            strcpy(namafile, dir_name);
            strcat(namafile, entry->d_name);
            if (remove(namafile) == 0) {
                printf("Menghapus %s.\n", namafile);
            } else {
                printf("Gagal menghapus %s.\n", namafile);
            }
        }
    }     
    closedir(dir);
```

##### Step 3
Pertama-tama membuat folder dengan int ffolder = fork();, lalu kita declare "char *args[]" yang digunakan sebagai argumen dari execvp yang akan digunakan nanti. Didalam parameter char *args[] terdapat fungsi linux mkdir untuk membuat folder, dilanjutkan dengan tempat direktori folder yang akan dibuat serta nama folder, sebagai contoh "./players/Bek" dimana "." itu merupakan directory saat ini, "/players/Bek" merupakan di dalam directory players yang ada di directory saat ini akan di buatkan folder yang bernama Bek. lalu NULL.

Lalu didalam parent process dari ffolder akan menunggu child prosesnya selesai terlebih dahulu, baru parent process di execute. Di dalam child process, akan dilakukan execvp dengan parameters "(args[0], args)" yang dimana args[0] merujuk pada parameter pertama dari args yaitu "mkdir", lalu "args" akan mengambil semua parameter di dalam char *agrs[], jadi didalam execvp itu akan memiliki parameters seperti ini :
execvp("mkdir", "mkdir", "./players/Bek", "./players/Gelandang", "./players/Kiper", "./players/Penyerang", NULL);

```
printf("Creating folder........\n");
    int ffolder = fork();
    char *args[] = {"mkdir", "./players/Bek", "./players/Gelandang", "./players/Kiper", "./players/Penyerang", NULL};
    if (ffolder == 0){
        printf("test");
         execvp(args[0], args);
         exit(1);
    }
    else
    {
        int status;
        wait(&status);
    }
    printf("Folder Created.\n");
```

Setelah folder berhasil dibuat, maka akan melakukan klasifikasi nama file yang akan dimasukan di masing masing folder dengan nama tertentu.
Program akan melakukan 4 buah fork yang dimana child process pada clasify1 akan di lakukan fork, lalu child process pada clasify2 akan di lakukan fork, lalu child process pada clasify3 akan di lakukan fork.

Pada child process clasify4, akan dilakukan fungsi execl dengan parameters "execl("/bin/sh", "sh", "-c", "mv ./players/*Bek* ./players/Bek/", NULL);" dimana execl akan menggunakan fungsi bash shell pada linux dengan memanggil path pada /bin/sh, lalu "-c" merupakan command pada sh untuk mengeksekusi perintah yang diberikan dalam bentuk string, lalu "mv ./players/*Bek* ./players/Bek/" merupakan command untuk memindahkan semua file yang memiliki nama atau string "Bek" pada nama file-nya di dalam direktori ./players ke dalam direktori ./players/Bek.

Pada parent process clasify4, akan dilakukan hal yang sama namun dengan nama atau string "Gelandang" pada nama file-nya di dalam direktori ./players ke dalam direktori ./players/Gelandang.

Pada parent process clasify3, akan dilakukan hal yang sama namun dengan nama atau string "Penyerang" pada nama file-nya di dalam direktori ./players ke dalam direktori ./players/Penyerang.

Pada parent process clasify2, akan dilakukan hal yang sama namun dengan nama atau string "Kiper" pada nama file-nya di dalam direktori ./players ke dalam direktori ./players/Kiper.

Pada parent process clasify1, akan menunggu child process clasify1 selesai yang dimana berisi perintah dengan struktur eksekusi diatas untuk selesai, lalu parent process clasify1 akan di eksekusi.

```
pid_t clasify1 = fork();
    if (clasify1 == 0)
    {
        pid_t clasify2 = fork();
        if(clasify2 == 0)
        {
            pid_t clasify3 = fork();
            if(clasify3 == 0)
            {
                pid_t clasify4 = fork();
                if(clasify4 == 0)
                {
                    execl("/bin/sh", "sh", "-c", "mv ./players/*Bek* ./players/Bek/", NULL);
                }
                else
                {
                    execl("/bin/sh", "sh", "-c", "mv ./players/*Gelandang* ./players/Gelandang/", NULL);
                }
            }
            else
            {
                execl("/bin/sh", "sh", "-c", "mv ./players/*Penyerang* ./players/Penyerang/", NULL);
            }
        }
        else
        {
            execl("/bin/sh", "sh", "-c", "mv ./players/*Kiper* ./players/Kiper/", NULL);
        }
    }
    else
    {
        int status;
        wait(&status);
    }
    printf("Succsesfully\n");
```

##### Step 4
Pertama-tama kita akan membuat fungsi yang bernama buatTim di luar int main() dengan parameter (int b, int g, int p)

Lalu declare k = 1; dan declare char newfile[200]; yang dimana akan di gunakan di seluruh fungsi buatTim.
Lalu program akan melakukan fungsi sprintf yang berguna untuk memasukan string ke dalam varibel dengan menambahkan nilai variabel pada string. "/home/tom/Formasi_%d-%d-%d.txt", b, g, p akan di masukan ke dalam newfile.

```
    int k = 1;
    char newfile[200];
    sprintf(newfile, "/home/tom/Formasi_%d-%d-%d.txt", b, g, p);
```

Lalu program akan membuat pid_file = fork();. Child process dari pid file akan menjalankan execl dengan parameter execl("/usr/bin/touch", "/usr/bin/touch", newfile, NULL); dimana didalam parameter execl berisi path dari fungsi touch yang  dan menjalankan fungsi touch pada linux yang berfungsi untuk membuat file. Lalu isi string dalam newfile akan menjadi lokasi dari file yang akan di buat dan nama filenya.
Lalu pada parent processnya akan menunggu child process selesai terlebih dahulu, setelah itu parent process akan di execute.

```
pid_t pid_file = fork();
    if (pid_file == 0)
    {
        execl("/usr/bin/touch", "/usr/bin/touch", newfile, NULL);
    }
    else
    {
        int status;
        waitpid(pid_file, &status, 0);
    }
```

Lalu program akan melakukan 4 buah fork yang dimana child process pada pid_kiper akan di lakukan fork, lalu child process pada pid_gelandang akan di lakukan fork, lalu child process pada pid_penyerang akan di lakukan fork.

Child process pada pid_bek akan menjalankan sprintf dengan parameters "sprintf(newfile, "ls -1 players/Bek/*.png | sort -n -r -t _ -k 4 | head -n %d >> /home/tom/Formasi_%d-%d-%d.txt", b, b, g, p);. "newfile" akan menjadi tempat untuk menyimpan string. "ls -1 players/Bek/*.png" adalah perintah untuk menampilkan semua file dengan format.png di dalam direktori players/Bek/. "sort -n -r -t _ -k 4" adalah perintah untuk menyortir file tersebut secara numerik (-n) secara reverse (-r) dengan menggunakan separator "_" (-t _ ) dan menggunakan kolom ke-4 sebagai acuan urutan (-k 4)."head -n %d" adalah perintah untuk menampilkan n baris teratas dari hasil pengurutan sebelumnya, dengan %d diisi dengan nilai variabel b. ">> /home/tom/Formasi_%d-%d-%d.txt" adalah perintah untuk menambahkan output dari perintah sebelumnya ke dalam file "/home/tom/Formasi_%d-%d-%d.txt. %d" akan diganti dengan nilai variabel b, g, dan p.
Lalu akan menjalankan perintah execl yang berisi parameter "execl("/usr/bin/sh", "/usr/bin/sh", "-c", newfile, NULL);" yang dimana pada paramater ini akan mencari path dari fungsi sh, di lanjutkan akan menggunakan fungsi sh pada execl, lalu "-c" sebagai penanda bahwa selanjutnya akan dianggap sebagai string perintah untuk di eksekusi, lalu NULL. 


Pada parent process pid_bek akan menjalankan hal yang sama dengan child process pid_bek namun yang membedakan hanya nama direktori untuk mencari .png akan berubah menjadi "players/Penyerang/*.png"

Pada parent process pid_penyerang akan menjalankan hal yang sama dengan child process pid_bek namun yang membedakan hanya nama direktori untuk mencari .png akan berubah menjadi "players/Gelandang/*.png"

Pada parent process pid_gelandang akan menjalankan hal yang sama dengan child process pid_bek namun yang membedakan hanya nama direktori untuk mencari .png akan berubah menjadi "players/Kiper/*.png"

Pada parent process pid_kiper, akan menunggu child process pid_kiper selesai yang dimana berisi perintah dengan struktur eksekusi diatas untuk selesai, lalu parent process pid_kiper akan di eksekusi.

```
pid_t pid_kiper = fork();
    if(pid_kiper == 0)
    {
        pid_t pid_gelandang = fork();
        if(pid_gelandang == 0)
        {
            pid_t pid_penyerang = fork();
            if(pid_penyerang == 0)
            {
                pid_t pid_bek = fork();
                if(pid_bek == 0)
                {
                    sprintf(newfile, "ls -1 players/Bek/*.png | sort -n -r -t _ -k 4 | head -n %d >> /home/tom/Formasi_%d-%d-%d.txt", b, b, g, p);
                    execl("/usr/bin/sh", "/usr/bin/sh", "-c", newfile, NULL);
                }
                else
                {
                    sprintf(newfile, "ls -1 players/Penyerang/*.png | sort -n -r -t _ -k 4 | head -n %d >> /home/tom/Formasi_%d-%d-%d.txt", p, b, g, p);
                    execl("/usr/bin/sh", "/usr/bin/sh", "-c", newfile, NULL);
                }
            }
            else
            {
                sprintf(newfile, "ls -1 players/Gelandang/*.png | sort -n -r -t _ -k 4 | head -n %d >> /home/tom/Formasi_%d-%d-%d.txt", g, b, g, p);
                execl("/usr/bin/sh", "/usr/bin/sh", "-c", newfile, NULL);
            }
        }
        else
        {
            sprintf(newfile, "ls -1 players/Kiper/*.png | sort -n -r -t _ -k 4 | head -n %d >> /home/tom/Formasi_%d-%d-%d.txt", k, b, g, p);
            execl("/usr/bin/sh", "/usr/bin/sh", "-c", newfile, NULL);
        }
    }
    else
    {
        int status;
        waitpid(pid_file, &status, 0);
    }
```

Setelah membuat fungsi buatTim, kita akan mendeclare nilai dari int b, g, dan p lalu kita akan panggil fungsi buatTim di dalam int main() dengan cara "buatTim(b,g,p);" dan program akan memanggil fungsi buatTim dan memasukan nilai variabel dari parameter buatTim dengan nilai dari int b, g dan p yang sudah di declare sebelum memanggil fungsi buatTim.

```
    int b = 4;
    int g = 4;
    int p = 2;
    
    buatTim(b,g,p);
```
## Soal 4
### Penjelasan Umum
Kita akan membuat kode yang akan menjalankan sebuah file shell dengan sistem seperti cron job, dimana program akan berjalan di spesifikasi waktu tertentu. Nah dikarenakan kita perlu mengecek setiap saat apakah waktu sudah sesuai agar tau kapan menjalankan kode maka kita perlu membuat proses daemon yang akan mengeksekusi file yang diinginkan.
### Input waktu dan file
Dalam penginputan waktu dan file untuk cron, kita perlu mendeteksi mana yang jem, menit, detik dan path filenya. lalu kita bisa membaca isi dan menggunakan sesuai dengan kriteria yang ditentukan. Untuk jam hanya bisa menerima 0-23 dan * dimana * berupa setiap jam. Untuk menit dan detik menerima 0-59 dan menerima * untuk respektif setiap menit/detik. selain yang ditentukan tidak diterima. Agar bisa membaca spesifikasi tersebut kita perlu menggunakan `int main(int argc, char *argv[])` yang berupa modifikasi int main yang berfungsi seperti command $1. $2 dst yang membaca input yang ada saat file dirun. argc yang menghitung banyak string yang diinput dan argv yang menjadikannya array string. 
```
int main(int argc, char *argv[]) {
 pid_t pid1, pid2,sd;
 char *filepath;
 int h,m,s;
 char *hr=argv[1], *mn=argv[2], *sc=argv[3];
if( hr[0] == '*'){
	h = -1;
} 
else if (isdigit(hr[0])){
int value=atoi(hr);
if(value<0 || value>59){printf("Error hour input\n");}
else {h = value;}	
}else {printf("Error hour input\n");}
if( mn[0] == '*'){
	m = -1;
}else if (isdigit(mn[0])){
	int value=atoi(mn);
	if(value<0 || value>59){printf("Error minute input\n");}
	else {m = value;}	
}else {printf("Error minute input\n");}
if( sc[0] == '*'){
	s = -1;
}else if (isdigit(sc[0])){
	int value=atoi(sc);
	if(value<0 || value>59){printf("Error second input\n");}
	else {s = value;}	
}else {printf("Error second input\n");}

 filepath = argv[4];
 filepath[strcspn(filepath, "\n")] = 0;
```
disini sesuai dengan argv, argv 1 akan berisi string spesifikasi jam, argv 2 berisi menit dan argv 3 berisi detik sementara argv 4 bersisi path fike yang akan langsung dijadikan string filepath.
Kita mengecek individu jam menit dan detik apakah mereka *, jika ya maka mereka diberi value -1 dikarenakan int yang dipakai selain * positif semua. lalu kita mengecek apakah string tersebut berupa digit/angka dengan isdigit, jika iya maka kita ubah menjadi int dengan atoi dan mengecek apakah sesuai dengan range yang ditentukan. Jika iya value jam menit dan detik akan sesuai dengan value input.
### DAEMON
 Dalam daemon kita perlu melakukan 2 hal, yaitu mengecek local time apakah sesuai dengan spesifikasi input, dan mengeksekusi apabila sesuai dengan input tersebut. dikarenakan kita hanya perlu mengeksekusi jika sesuai dengan input, maka kita hanya melakukan fork dalam daemon saat sesuai agar tidak membuat child terus menerus.
```
 pid1 = fork();

  if (pid1 < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid1 > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sd = setsid();
  if (sd < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

while(1){ 
		
            time_t ctime = time(NULL);
            struct tm *tnow = localtime(&ctime);
            if(( h== -1 || tnow ->tm_hour == h)
             && ( m== -1 || tnow ->tm_min == m)
              && ( s== -1 || tnow ->tm_sec == s)) { //cek waktu
              pid2=fork();
            	if(pid2==0){
                char *args[]= {"/bin/sh", filepath, NULL};
 		int val = execvp(args[0], args);
 		if (val=-1) {
			printf("fail to exec");
 		}
		 exit(0);
            }
            }
            sleep(1); //tidur selama 1 detik 

```
Buat pid dan sid daemon dan di umask agar bisa lancar. lalu dalam daemon kita mendapatkan nilai local time dengan library time.h dan `time_t ctime = time(NULL); struct tm *tnow = localtime(&ctime);` yang akan mendapatkan local time. Dalam mengecek kita perlu menentukan apakah jam, menit dan detik local sesuai dengan input. disini kita menggunakn "-> format" yang akan mengambil format spesifik dan membandingkannya dengan value yang didapat dari input. Jika sesuai kita lalu fork dan buat string args yang berisi command yang akan dieksekusi, yaitu library shell dan path file shellnya. kita lalu mengeksekusi file tersebut. 
### KODE
```
int main(int argc, char *argv[]) {
 pid_t pid1, pid2,sd;
 char *filepath;
 int h,m,s;
 char *hr=argv[1], *mn=argv[2], *sc=argv[3];
if( hr[0] == '*'){
	h = -1;
} 
else if (isdigit(hr[0])){
int value=atoi(hr);
if(value<0 || value>59){printf("Error hour input\n");}
else {h = value;}	
}else {printf("Error hour input\n");}
if( mn[0] == '*'){
	m = -1;
}else if (isdigit(mn[0])){
	int value=atoi(mn);
	if(value<0 || value>59){printf("Error minute input\n");}
	else {m = value;}	
}else {printf("Error minute input\n");}
if( sc[0] == '*'){
	s = -1;
}else if (isdigit(sc[0])){
	int value=atoi(sc);
	if(value<0 || value>59){printf("Error second input\n");}
	else {s = value;}	
}else {printf("Error second input\n");}

 filepath = argv[4];
 filepath[strcspn(filepath, "\n")] = 0;
  pid1 = fork();

  if (pid1 < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid1 > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sd = setsid();
  if (sd < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

while(1){ 
		
            time_t ctime = time(NULL);
            struct tm *tnow = localtime(&ctime);
            if(( h== -1 || tnow ->tm_hour == h)
             && ( m== -1 || tnow ->tm_min == m)
              && ( s== -1 || tnow ->tm_sec == s)) { //cek waktu
              pid2=fork();
            	if(pid2==0){
                char *args[]= {"/bin/sh", filepath, NULL};
 		int val = execvp(args[0], args);
 		if (val=-1) {
			printf("fail to exec");
 		}
		 exit(0);
            }
            }
            sleep(1); //tidur selama 1 detik 
}
 return 0;
}
```


