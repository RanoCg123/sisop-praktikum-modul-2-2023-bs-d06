#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <time.h>

void zip(char *zipfile, char *target) {
  pid_t zfork;
  zfork = fork();
  char *argv[] = {"zip", "-q", "-rm", zipfile, target, NULL};
  if(zfork == 0) {
    execv("/usr/bin/zip", argv);
  }
  while(wait(NULL) != zfork);
}

void random_pick() {
  DIR *opdir = opendir("./kebun");
  struct dirent *rdir;
  if(opdir != NULL) {
    int total_files = 0;
    while((rdir = readdir(opdir)) != NULL ) {
      if(rdir->d_type == DT_REG) {
	total_files++;
      }
    }
    srand(time(NULL));
    int num_rand = rand() % total_files;
    rewinddir(opdir);
    int count = 0;
    while((rdir = readdir(opdir)) != NULL) {
      if(rdir->d_type == DT_REG) {
	if(count == num_rand) {
	  printf("Hewan yang dijaga saat ini adalah %s\n", rdir->d_name);
	}
	count++;
      }
    }
    closedir(opdir);
  }
}

void unzip() {
  pid_t uzfork;
  uzfork = fork();
  char *argv[] = {"unzip", "-q", "binatang.zip", "-d", "kebun", NULL};
  if(uzfork == 0) {
    execv("/bin/unzip", argv);
  }
  while(wait(NULL) != uzfork);
  remove("binatang.zip");
}

void move_file(char *destination, char *location) {
  pid_t mvfork;
  mvfork = fork();
  char *argv[] = {"mv", location, destination, NULL};
  if(mvfork == 0) {
    execv("/bin/mv", argv);
  }
  while(wait(NULL) != mvfork);
}

void file_to_new_dir() {
  DIR *opdir = opendir("./kebun");
  struct dirent *rdir;
  if(opdir != NULL) {
    while((rdir = readdir(opdir)) != NULL) {
      char location[255] = "kebun/";
      strcat(location, rdir->d_name);
      if(strstr(rdir->d_name, ".jpg") != NULL) {
        if(strstr(rdir->d_name, "darat") != NULL) {
	  move_file("HewanDarat/", location);
	}
	else if(strstr(rdir->d_name, "amphibi") != NULL) {
          move_file("HewanAmphibi/", location);
        }
        else if(strstr(rdir->d_name, "air") != NULL) {
          move_file("HewanAir/", location);
        }
      }
    }
    closedir(opdir);
  }
  remove("kebun");
}

void create_folder() {
  pid_t mkdfork;
  mkdfork = fork();
  char *argv[] = {"mkdir", "kebun", "HewanDarat", "HewanAmphibi", "HewanAir", NULL};
  if(mkdfork == 0) {
    execv("/bin/mkdir", argv);
  }
  while(wait(NULL) != mkdfork);
}

void download() {
  pid_t dlfork;
  dlfork = fork();
  char *argv[] = {"wget", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", "binatang.zip", NULL};
  if(dlfork == 0) {
    execv("/usr/bin/wget", argv);
  }
  while(wait(NULL) != dlfork);
}

int main (void) {
  pid_t main_fork;
  download();
  main_fork = fork();
  if(main_fork == 0) {
    create_folder();
    exit(0);
  }
  while(wait(NULL) != main_fork);
  main_fork = fork();
  if(main_fork == 0) {
    unzip();
    exit(0);
  }
  while(wait(NULL) != main_fork);

  main_fork = fork();
  if(main_fork == 0) {
    random_pick();
    exit(0);
  }
  while(wait(NULL) != main_fork);

  main_fork = fork();
  if(main_fork == 0) {
    file_to_new_dir();
    exit(0);
  }
  while(wait(NULL) != main_fork);

  main_fork = fork();
  if(main_fork == 0) {
    zip("HewanDarat.zip", "HewanDarat");
    zip("HewanAmphibi.zip", "HewanAmphibi");
    zip("HewanAir.zip", "HewanAir");
    exit(0);
  }
  while(wait(NULL) != main_fork);

  return 0;
}
