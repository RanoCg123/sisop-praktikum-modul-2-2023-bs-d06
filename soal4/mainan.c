#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

int main(int argc, char *argv[]) {
 pid_t pid1, pid2,sd;
 char *filepath;
 int h,m,s;
 char *hr=argv[1], *mn=argv[2], *sc=argv[3];
if( hr[0] == '*'){
	h = -1;
} 
else if (isdigit(hr[0])){
int value=atoi(hr);
if(value<0 || value>23){printf("Error hour input\n"); return 0;}
else {h = value;}	
}else {printf("Error hour input\n");return 0;}
if( mn[0] == '*'){
	m = -1;
}else if (isdigit(mn[0])){
	int value=atoi(mn);
	if(value<0 || value>59){printf("Error minute input\n"); return 0;}
	else {m = value;}	
}else {printf("Error minute input\n");return 0;}
if( sc[0] == '*'){
	s = -1;
}else if (isdigit(sc[0])){
	int value=atoi(sc);
	if(value<0 || value>59){printf("Error second input\n"); return 0;}
	else {s = value;}	
}else {printf("Error second input\n");return 0;}

 filepath = argv[4];
 filepath[strcspn(filepath, "\n")] = 0;
  pid1 = fork();

  if (pid1 < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid1 > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sd = setsid();
  if (sd < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

while(1){ 
		
            time_t ctime = time(NULL);
            struct tm *tnow = localtime(&ctime);
            if(( h== -1 || tnow ->tm_hour == h)
             && ( m== -1 || tnow ->tm_min == m)
              && ( s== -1 || tnow ->tm_sec == s)) { //cek waktu
              pid2=fork();
            	if(pid2==0){
                char *args[]= {"/bin/sh", filepath, NULL};
 		int val = execvp(args[0], args);
 		if (val=-1) {
			printf("fail to exec");
 		}
		 exit(0);
            }
            }
            sleep(1); //tidur selama 1 detik 
}
 return 0;
}


