#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include<wait.h>
void makekill(char order[], char kp[]){
 FILE *kl;
 char isi[]= "#include <stdio.h>\n" "#include <unistd.h>\n"
     "int main() {\n"
        "pid_t kill = fork();\n"
        "if (kill == 0) {\n"
            "%s"
            "execv(\"%s\", argv);\n"
        "}\n"
        "pid_t ks = fork();\n"
        "if (ks == 0) {\n"
        "char *argv[] = {\"rm\", \"kys\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"
         "}\n"
        "pid_t kys = fork();\n"
        "if (kys == 0) {\n"
        "char *args[] = {\"rm\", \"kys.c\", NULL};\n"
        "execv(\"/bin/rm\", args);\n"
        "}\n"
        "return 0;\n"
    "}\n";
 kl = fopen("kys.c", "w");
 fprintf(kl, isi, order, kp);
 fclose(kl);
 pid_t kys;
 kys=fork();
 if(kys==0){
char *argv[]={"gcc","kys.c","-o","kys", NULL};
execv("/bin/gcc",argv);
}
}
void dlimage(char *fname){
char gname[30], path[100], dl[100];
pid_t pd;
for(int i = 0; i < 15; i++)
    {
            time_t ct = time(NULL);
            struct tm* tn = localtime(&ct);
            strftime(gname, 30, "%Y-%m-%d_%H:%M:%S", tn);
            sprintf(path, "./%s/%s.jpg", fname,gname);
            int size=(ct%1000)+50;
            sprintf(dl, "https://picsum.photos/%d", size);
            char *args[] = {"wget", "-q", "-O", path, dl, NULL};
        pd = fork();
        if(pd == 0)
        {
            execv("/bin/wget", args);
        }
        sleep(5);
    }
    
}
void mkfile(char *fname){
pid_t pd;
pd = fork();
char *argv[] = {"mkdir", "-p", fname, NULL};
  if(pd == 0){
      dlimage(fname);
  } 
  else{ 
    execv("/bin/mkdir", argv);
  }
}
void rmfile(char* fname)
{
    char *argv[] = {"rm", "-r", fname, NULL};
    execv("/bin/rm", argv);
}
void zpfile(char* fname)
{
    char zfile[50];
    sprintf(zfile, "%s.zip", fname);
    pid_t zfork,rfork;
    zfork = fork();
    char *argv[] = {"zip","-q", "-r", zfile, fname, NULL};
    if(zfork == 0)
    {
         execv("/bin/zip", argv );
    }
    while(wait(NULL)!=zfork);
    rmfile(fname);
}




int main(int argc, char *argv[]){
pid_t pd, sd;
char fname[30], *type[]={"-a","-b"},mode[101],kp[101];

  pd = fork();
 if (pd < 0) {
    exit(EXIT_FAILURE);
  }
  if (pd > 0) {
    exit(EXIT_SUCCESS);
  }
  umask(0);
  sd = setsid();
  if (sd < 0) {
    exit(EXIT_FAILURE);
  }
  if (strcmp(argv[1],type[0])==0){
 sprintf(mode,"char *argv[] = {\"killall\",\"-q\",\"lukisan\", NULL};");
 sprintf(kp,"/bin/killall");}
if (strcmp(argv[1],type[1])==0){
sprintf(mode,"char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
sprintf(kp,"/bin/kill");}
makekill(mode,kp);
close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);
while(1){
pid_t make;
time_t ctime = time(NULL);
struct tm *tnow = localtime(&ctime);
strftime(fname,80,"%Y-%m-%d_%H:%M:%S", tnow);
make = fork();
if(make == 0)
    {
   mkfile(fname);
   zpfile(fname);
    }

sleep(30);
}
return 0;
}
